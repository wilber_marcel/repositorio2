#!/usr/bin/env bash

# Set variables
REPOSITORY=$1
TAG1=$2
TAG2=$3
TAG3=$4
TAG4=$5
TAG5=$6

function deleteTag () {
  curl -X DELETE https://api.bitbucket.org/2.0/repositories/$BITBUCKET_REPO_OWNER/$REPOSITORY/refs/tags/release-$TAG \
  -u wilber_marcel:GzDvKfkwsqPEQwuNju87 \
  --fail --show-error --silent 
}

if [ -n "$REPOSITORY" ]; then
    if [ -n "$TAG1" ]; then
      echo "TAG-1 = " $TAG1
      TAG=$TAG1
      echo "TAGG = " $TAG
      deleteTag
    fi
    if [ -n "$TAG2" ]; then
      TAG=$TAG2
      deleteTag
    fi
    if [ -n "$TAG3" ]; then
      TAG=$TAG3
      deleteTag
    fi
    if [ -n "$TAG4" ]; then
      TAG=$TAG4
      deleteTag
    fi
    if [ -n "$TAG5" ]; then
      TAG=$TAG5
      deleteTag
    fi
else
  echo "The 'Repository' parameter is missing"
fi