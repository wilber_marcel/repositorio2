#!/usr/bin/env bash

# Exit immediately if a any command exits with a non-zero status
set -e

# Set variables
REPOSITORY=$1

curl https://api.bitbucket.org/2.0/repositories/wilber_marcel/magis5-core/commits/master?pagelen=1 \
-u wilber_marcel:GzDvKfkwsqPEQwuNju87 -H 'Accept: application/json' -# -o arq1.hash
sed 's/{"pagelen.*, "hash//' arq1.hash > arq2.hash
grep -i "" arq2.hash | awk '{print $2}' > arq3.hash
sed 's/"//g' arq3.hash > arq4.hash
sed 's/,//g' arq4.hash > arq5.hash
LAST_COMMIT_HASH=$(cat arq5.hash)
rm -f arq*.hash
echo "Last Commit: " $LAST_COMMIT_HASH

# Create new pull request and get its ID
echo "Creating Tag1111"
curl -X POST https://api.bitbucket.org/2.0/repositories/wilber_marcel/$REPOSITORY/refs/tags \
  -u wilber_marcel:GzDvKfkwsqPEQwuNju87 \
  --fail --show-error --silent \
  -H 'Content-Type: application/json' \
  -d '{
      "name": "release-'$BITBUCKET_BUILD_NUMBER'",
        "target": {
                "hash": "'$LAST_COMMIT_HASH'"
      }
  }'